from pyspark.sql import SparkSession, DataFrame

from pyspark.sql.functions import col

from land_storage.configuration.config import Config
from land_storage.fuel_changes.fuel_changes import get_aggregated_fuel_changes


class Report:
    @classmethod
    def main(cls, spark: SparkSession, conf: Config) -> DataFrame:
        movements, storage = cls.get_data(spark, conf)

        storage_limits = cls.get_storage_limits(storage)
        fuel_changes = get_aggregated_fuel_changes(movements=movements, storage=storage, dt=conf.report_dt)
        # transforming the changes for easy join
        fuel_changes = fuel_changes.groupBy("port").pivot("product").sum("change")

        df = cls.get_result_data(storage_limits=storage_limits, fuel_changes=fuel_changes)
        cls.save_csv(df, conf)
        return df

    @classmethod
    def get_data(cls, spark: SparkSession, conf: Config) -> (DataFrame, DataFrame):
        """Get initial data"""
        movements = spark.read.parquet(conf.movements_path)
        storage = spark.read.parquet(conf.storage_path)
        return movements, storage

    @classmethod
    def get_storage_limits(cls, storage: DataFrame) -> DataFrame:
        return storage.withColumn(
            "crude_limit", col("crude_absolute") / col("crude_relative")
        ).withColumn(
            "diesel_limit", col("diesel_absolute") / col("diesel_relative")
        )

    @classmethod
    def get_result_data(cls, storage_limits: DataFrame, fuel_changes: DataFrame) -> DataFrame:
        return storage_limits.join(
            fuel_changes, "port", how="inner"
        ).drop(
            fuel_changes["port"]
        ).selectExpr(
            "port",
            "crude_absolute+`crude oil` as crude_absolute",
            "(crude_absolute+`crude oil`)/crude_limit as crude_relative",
            "diesel_absolute+diesel as diesel_absolute",
            "(diesel_absolute+diesel)/diesel_limit as diesel_relative",
        )

    @classmethod
    def save_csv(cls, df: DataFrame, conf: Config):
        """Write csv file"""
        if conf.target.file_path:
            df.write.csv(conf.target.file_path, mode="overwrite", header=True)
