from datetime import datetime
from pathlib import PosixPath

from pyspark.sql import SparkSession

from pyspark import SparkConf

from land_storage.configuration.config import Config
from land_storage.report import Report


def get_spark_conf() -> SparkConf:
    config = SparkConf().setAll(
        [
            ("spark.app.name", f"{__name__}"),
        ]
    )
    return config


def main():
    spark_config = get_spark_conf()
    spark = SparkSession.builder.config(conf=spark_config).getOrCreate()
    project_path = PosixPath(__file__).parent.parent.parent
    movement_file = project_path / "data/cargo_movements.parquet"
    storage_file = project_path / "data/storage_asof_20200101.parquet"
    config = Config.from_mapping(
        {
            "storage_path": storage_file.absolute().as_posix(),
            "movements_path": movement_file.absolute().as_posix(),
            "target": {
                "path": project_path.absolute().as_posix(),
                "name": "storage_asof_20200114.csv",
            },
            "report_dt": datetime(2020, 1, 14),
        }
    )
    Report.main(spark, config)


if __name__ == "__main__":
    main()
