from datetime import datetime, date
from typing import Sequence

from pyspark.sql import DataFrame
from pyspark.sql.functions import col, lit, row_number, sum
from pyspark.sql.window import Window


def get_aggregated_fuel_changes(
    movements: DataFrame,
    storage: DataFrame,
    dt: date | datetime,
    fuel_types: Sequence[str] = None,
) -> DataFrame:
    fuel_changes = get_fuel_changes(movements=movements, storage=storage, dt=dt, fuel_types=fuel_types)
    window = Window.partitionBy(
        fuel_changes.port,
        fuel_changes.product
    ).orderBy(
        fuel_changes.event_dttm
    ).rowsBetween(
        start=Window.unboundedPreceding,
        end=Window.unboundedFollowing,
    )

    rn_window = Window.partitionBy(
        fuel_changes.port,
        fuel_changes.product
    ).orderBy(
        fuel_changes.event_dttm
    )

    # calculate fuel changes over unbound window
    fuel_changes = fuel_changes.withColumn(
        "abs_changes", sum("quantity").over(window)
    ).withColumn(
        "rn", row_number().over(rn_window)
    )

    # we need the aggregation result only
    fuel_changes = fuel_changes.filter(
        col("rn") == 1
    ).select(
        col("port"), col("product"), col("abs_changes").alias("change")
    )
    return fuel_changes


def get_fuel_changes(
    movements: DataFrame,
    storage: DataFrame,
    dt: date | datetime,
    fuel_types: Sequence[str] = None,
) -> DataFrame:
    fuel_increases = get_fuel_increases(movements=movements, storage=storage, dt=dt, fuel_types=fuel_types)
    fuel_decreases = get_fuel_decreases(movements=movements, storage=storage, dt=dt, fuel_types=fuel_types)
    return fuel_increases.unionAll(fuel_decreases)


def get_fuel_increases(
    movements: DataFrame,
    storage: DataFrame,
    dt: date | datetime,
    fuel_types: Sequence[str] = None,
) -> DataFrame:
    fuel_types = fuel_types or ["crude oil", "diesel"]
    fuel_increases = movements.join(
        storage, on=storage["port"] == movements["discharge_port"], how="inner"
    ).filter(
        col("product").isin(fuel_types)
    ).filter(
        col("end_timestamp") <= lit(dt)
    ).select(
        [
            col("discharge_port").alias("port"),
            col("quantity"),
            col("product"),
            col("end_timestamp").alias("event_dttm"),
        ]
    )
    return fuel_increases


def get_fuel_decreases(
    movements: DataFrame,
    storage: DataFrame,
    dt: date | datetime,
    fuel_types: Sequence[str] = None,
) -> DataFrame:
    fuel_types = fuel_types or ["crude oil", "diesel"]
    fuel_decreases = movements.join(
        storage, on=storage["port"] == movements["loading_port"], how="inner"
    ).filter(
        col("product").isin(fuel_types)
    ).filter(
        col("start_timestamp") <= lit(dt)
    ).withColumn(
        "quantity", col("quantity") * (-1)
    ).select(
        [
            col("loading_port").alias("port"),
            col("quantity"),
            col("product"),
            col("start_timestamp").alias("event_dttm"),
        ]
    )
    return fuel_decreases
