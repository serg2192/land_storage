from dataclasses import dataclass, is_dataclass, field
from datetime import date, datetime
from typing import MutableMapping


@dataclass
class Target:
    path: str
    name: str

    @property
    def file_path(self) -> str:
        path = None
        if self.path and self.name:
            path = f"{self.path}/{self.name}"
        return path

    def __bool__(self):
        return bool(self.file_path)


@dataclass
class Config:
    storage_path: str
    movements_path: str
    target: Target
    report_dt: date | datetime

    def __post_init__(self):
        for attr_name, attr_type in self.__annotations__.items():
            if is_dataclass(attr_type):
                setattr(self, attr_name, attr_type(**getattr(self, attr_name)))

    @classmethod
    def from_mapping(cls, mapping: MutableMapping):
        return cls(**mapping)
