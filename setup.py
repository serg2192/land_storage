from pathlib import Path

from setuptools import setup, find_packages

SELF_PATH = Path(__file__).parent.absolute()


if __name__ == '__main__':
    with open(SELF_PATH / 'README.md', encoding='utf8') as fd:
        long_description = fd.read()

    with open(SELF_PATH / 'requirements.txt', encoding='utf8') as fd:
        install_requires = [li for li in fd.readlines() if li and not li.startswith('#')]

    with open(SELF_PATH / 'requirements-test.txt', encoding='utf8') as fd:
        test_requires = [li for li in fd.readlines() if li and not (li.startswith('#') or li.startswith('-r'))]

    setup(
        name='land-storage',
        url='https://TBD',
        version="0.1",
        author='Sergey Ondrin',
        author_email='sondrin@rambler.ru',
        description='Vortexa land storage',
        long_description=long_description,
        python_requires='>=3.10',
        install_requires=install_requires,
        include_package_data=True,
        package_dir={'': 'src'},
        packages=find_packages(where='src'),
        data_files=[('data', ['data/cargo_movements.parquet', 'data/storage_asof_20200101.parquet'])],
        extras_require={
            'test': test_requires,
        },
    )
