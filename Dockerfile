FROM python:3.10-slim

WORKDIR /opt/project

COPY setup.py requirements.txt requirements-test.txt README.md ./

RUN apt update && apt install -y openjdk-11-jre curl && pip install -r requirements-test.txt

COPY src /opt/project/src
COPY data /opt/project/data

RUN pip install --no-deps -e .

EXPOSE 4040
