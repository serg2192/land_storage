from datetime import datetime

import pytest

from land_storage.configuration.config import Config, Target


class TestTarget:
    def test_all(self):
        target_path = "test_path"
        target_name = "test_name"
        target = Target(path=target_path, name=target_name)
        assert target.path == target_path
        assert target.name == target_name
        assert target.file_path == f"{target_path}/{target_name}"

    @pytest.mark.parametrize(
        "target_path, target_name",
        [
            (None, "test"),
            ("test", None),
            (None, None),
            ("test", "test"),
        ]
    )
    def test_check_bool(self, target_path, target_name):
        target = Target(path=target_path, name=target_name)
        if target_path and target_name:
            assert target.file_path == f"{target_path}/{target_name}"
            assert bool(target)
        else:
            assert target.file_path is None
            assert not bool(target)


class TestConfig:
    def test_all(self):
        storage_path = "test_path"
        movements_path = "test_path"
        target_path = "path"
        target_name = "name"
        config = {
            "storage_path": storage_path,
            "movements_path": movements_path,
            "target": {
                "path": "path",
                "name": "name"
            },
            "report_dt": datetime(2022, 1, 1)
        }
        config = Config.from_mapping(config)
        assert config.movements_path == movements_path
        assert config.storage_path == storage_path
        assert config.target.file_path == f"{target_path}/{target_name}"
        assert config.report_dt == datetime(2022, 1, 1)
