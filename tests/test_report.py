from datetime import datetime
from pathlib import PosixPath

from pyspark.sql import SparkSession, DataFrame
from pyspark import SparkConf
from pyspark.sql.types import Row

from land_storage.configuration.config import Config
from land_storage.report import Report

PROJECT_PATH = PosixPath(__file__).parent.parent


def create_reference_df(spark: SparkSession) -> DataFrame:
    data = [
        Row(
            port='Singapore',
            crude_absolute=119830011,
            crude_relative=0.4992917125,
            diesel_absolute=14456409,
            diesel_relative=0.597531572,
        ),
        Row(
            port='Rotterdam',
            crude_absolute=41910325,
            crude_relative=0.32929541071428575,
            diesel_absolute=70898194,
            diesel_relative=0.7976046825,
        ),
        Row(
            port='Houston',
            crude_absolute=49656788,
            crude_relative=0.446911092,
            diesel_absolute=2092433,
            diesel_relative=0.30689017333333335,
        )
    ]
    return spark.createDataFrame(data)


class TestMain:
    spark: SparkSession = None

    @classmethod
    def setup_class(cls):
        conf = SparkConf().setAll(
            [
                ("spark.app.name", f"{cls.__name__}"),
            ]
        )
        cls.spark = SparkSession.builder.config(conf=conf).getOrCreate()

    def setup(self):
        self.movement_file = PROJECT_PATH / "data/cargo_movements.parquet"
        self.storage_file = PROJECT_PATH / "data/storage_asof_20200101.parquet"

    @classmethod
    def teardown_class(cls):
        cls.spark.stop()

    def test_all(self, tmp_path):
        self.config = Config.from_mapping(
            {
                "storage_path": self.storage_file.absolute().as_posix(),
                "movements_path": self.movement_file.absolute().as_posix(),
                "target": {
                    "path": tmp_path.absolute().as_posix(),
                    "name": "some_name.csv",
                },
                "report_dt": datetime(2020, 1, 14),
            }
        )
        reference_df = create_reference_df(self.spark)
        res = Report.main(self.spark, self.config)
        assert res.collect() == reference_df.collect()
