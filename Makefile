.PHONY: init run_test run_container test run
ROOT_DIR=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

init:
	docker image build -f ./Dockerfile --tag vortexa .

run_test:
	docker run -it --rm -v $(ROOT_DIR):/opt/project vortexa pytest

run_container:
	docker run -v $(ROOT_DIR):/opt/project --rm vortexa python3 -m land_storage.main

test: init run_test

run: init run_container
