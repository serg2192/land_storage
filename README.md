# Vortexa Programming Assignment - Land Storage

## Context:
We are interested in understanding how sea-borne flows of energy products impact on-land storage reserves.

For this exercise, please assume that the only way by which on-land storage changes is when a vessel loads a product from that port, or when a vessel discharges a product to that port.  

For example, during a cargo movement a vessel loads 100,000 Tons of crude oil from Houston, and discharges it in Rotterdam.  In this case, Houston’s Crude Oil reserves would decrease by 100,000 Tons, and Rotterdam’s would increase by 100,000 Tons.

## Data:
In the `data/` directory you will find 2 files:
* `storage_asof_20200101.parquet` - storage reserves of Crude Oil and Diesel for a set of ports as of 01/01/2020.  This includes the absolute quantity stored, as well as a percentage value, relative to the maximum capacity which the ports can hold for a given product.
* `cargo_movements.parquet` - A series of cargo movements from 01/01/2020 until the end of 14/01/2020.  Each cargo movement represents a vessel loading a product from an origin port (loading_port) and discharging it at a destination port (`discharge_port`).

## Task:
Please write a program which will return a csv file (`storage_asof_20200114.csv`) with the the absolute and relative storage quantities updated to reflect storage levels as of **14/01/2020 00:00:00**.

We are only interested in the storage levels of **Crude Oil and Diesel, at the ports for which we have an opening storage balance.**

Storage levels decrease at the loading port, as soon as the cargo movement starts (`start_timestamp`).  At the discharge port, storage levels only increase once the vessel arrives in port (`end_timestamp`).

## Requirements:
* The code should be written in a modular fashion, and to a production standard
* The program should run in docker

This assignment should take no longer than a couple of hours.

Good Luck!

## To Run:

To run code:
```shell
make run
```
or
```shell
docker image build -f ./Dockerfile --tag vortexa . && docker run -v $(pwd):/opt/project --rm vortexa python3 -m land_storage.main
```

To run tests:
```shell
make test
```
